require essioc
require crome

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet(IOCNAME, "$(IOCNAME)")

epicsEnvSet(DEVICE_HOSTNAME, "rems-amm-1005.tn.esss.lu.se")
epicsEnvSet(P, "REMS-SPKRS:")
epicsEnvSet(R, "RMT-AMM-001:")

iocshLoad("$(crome_DIR)/crome.iocsh", "DEVICE_HOSTNAME=$(DEVICE_HOSTNAME), P=$(P), R=$(R)")
